import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
// components
import * as fromComponents from './components';
// containers
import * as fromContainers from './containers';

// services
import * as fromServices from './services';
import { Routes, RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { effects, reducers } from './store';
import { EffectsModule } from '@ngrx/effects';

// routes
export const ROUTES: Routes = [
  {
    path: '',
    component: fromContainers.BooksComponent
  },
  {
    path: ':bookId',
    component: fromContainers.BooksItemComponent
  }
];
@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(ROUTES),
    StoreModule.forFeature('catalogs', reducers),
    EffectsModule.forFeature(effects)
  ],
 providers: [ ...fromServices.services ],
  declarations: [ ...fromContainers.containers, ...fromComponents.components ],
  exports: [ ...fromContainers.containers,  ...fromComponents.components ]
})
export class CatalogModule { }
