import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
@Component({
  selector: 'app-book-search',
  templateUrl: './book-search.component.html',
  styleUrls: ['./book-search.component.css']
})
export class BookSearchComponent implements OnInit {
  searchTerm: FormControl;
  @Output() searchEvent = new EventEmitter<string>();
  constructor() {}

  ngOnInit() {
    this.searchTerm = new FormControl();
    this.searchTerm.valueChanges
    .debounceTime(400)
    .distinctUntilChanged()
    .subscribe(term => {
        this.searchEvent.emit(term);
    });
  }
  sendSearchTerm() {

    this.searchTerm.valueChanges
    .debounceTime(400)
    .distinctUntilChanged()
    .subscribe(term => {
        this.searchEvent.emit(term);
    });
  }

}
