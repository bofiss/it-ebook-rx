import { MenuComponent } from './menu/menu.component';
import { BookSearchComponent } from './book-search/book-search.component';

export const components: any[] = [MenuComponent, BookSearchComponent];

export  *  from  './menu/menu.component';
export  *  from  './book-search/book-search.component';
