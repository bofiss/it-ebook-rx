import { Component, OnInit } from '@angular/core';
import { BookResponse } from '../../models/bookResponse.model';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import * as fromStore from '../../store';
import { Store } from '@ngrx/store';

import { Book } from '../../models/book.model';
import { BooksService } from '../../services/books.service';
import { getTotalPage } from '../../store/selectors/books.selector';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
  term: string;
  total: number;
  books$: Observable<Book[]>;
  booksResponse$: Observable<BookResponse>;
  constructor(private bookService: BooksService, private store: Store<fromStore.CatalogState>) {}

  ngOnInit() {

  }

 text_truncate(main_string, pos, character) {
    if ((typeof (main_string) === 'undefined') && (typeof (pos === 'undefined'))) {
    return main_string;
    }
    if (typeof (pos) === 'undefined') {
    pos = 0;
    }
    if (typeof (character) === 'undefined') {
    character = '...';
    }

    return main_string.substr(0, pos) + character;
  }
  getSearchTerm($event) {

    this.bookService.page = 1;
    this.bookService.setTerm($event);
    this.getBooks();
  }

  getBooks() {
    this.store.select(fromStore.getTotalPage).subscribe(total => this.total = total);
    console.log(this.total);
    this.books$ = this.store.select(fromStore.getAllBooks);
    this.store.dispatch(new fromStore.LoadBookResponse());
  }

  getMore() {
    if (this.bookService.page < this.total) {
      this.bookService.setPage();
      this.getBooks();
    }

  }






}
