import { BooksComponent } from './books/books.component';
import { BooksItemComponent } from './books-item/books-item.component';

export const containers: any[] = [BooksComponent, BooksItemComponent];

export * from './books/books.component';
export * from './books-item/books-item.component';
