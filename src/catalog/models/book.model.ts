export interface Book {
  ID: any;
  Title: string;
  SubTitle: string;
  Description: string;
  Image: string;
  isbn: string;
}
