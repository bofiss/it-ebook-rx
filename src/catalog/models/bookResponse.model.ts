import { Book } from './book.model';
export interface BookResponse {
  Error: string;
  Time: number;
  Total: string;
  Page: number;
  Books: Book[];
}
