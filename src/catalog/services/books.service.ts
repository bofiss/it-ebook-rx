import { BookResponse } from './../models/bookResponse.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { environment } from '../../environments/environment';
import { catchError } from 'rxjs/operators';

@Injectable()
export class BooksService {
term: string;
page = 0;
constructor(private httpClient: HttpClient) { }

getBooks(): Observable<BookResponse> {
  return this.httpClient.get<BookResponse>(environment.apiUrl + '/search/' + this.term + '/page/' + this.page)
  .pipe(catchError((error: any) => Observable.throw(error)));
}
getBookById(id: number): Observable<any> {
  return this.httpClient.get(environment.apiUrl + '/search/php');
}

setTerm(term) {
  this.term = term;
}

setPage() {
 this.page++;
}

}
