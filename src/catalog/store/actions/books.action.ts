import { BookResponse } from './../../models/bookResponse.model';
import { Action } from '@ngrx/store';
import { Book } from '../../models/book.model';
import { ResponseError } from '../../models/responseError.model';

// load pizza

export const LOAD_BOOKRESPONSE = '[BookResponse] Load BookResponse';
export const LOAD_BOOKRESPONSE_FAIL = '[BookResponse] Load BookResponse Fail';
export const LOAD_BOOKRESPONSE_SUCCESS = '[BookResponse] Load BookResponse Success';

export class LoadBookResponse implements Action {
  readonly type = LOAD_BOOKRESPONSE;
}

export class LoadBookResponseFail implements Action {
  readonly type = LOAD_BOOKRESPONSE_FAIL;
  constructor(public payload: any) {}
}

export class LoadBookResponseSuccess implements Action {
  readonly type = LOAD_BOOKRESPONSE_SUCCESS;
  constructor(public payload: BookResponse) {}
}

// action types

export type BookResponseAction = LoadBookResponse|LoadBookResponseFail|LoadBookResponseSuccess;
