import { LoadBookResponseSuccess } from './../actions/books.action';
import { of } from 'rxjs/observable/of';
import { Observable } from 'rxjs/Observable';
import { Action, Store } from '@ngrx/store';
import { Actions, Effect, toPayload } from '@ngrx/effects';
import * as bookActions from '../actions/books.action';
import { switchMap, map, catchError } from 'rxjs/operators';
import { BooksService } from '../../services/books.service';
import { Injectable } from '@angular/core';


@Injectable()
export class BooksEffects {

  constructor(
    private actions$: Actions,
    private booksService: BooksService
  ) {}

  @Effect()
  loadBooks$ = this.actions$
    .ofType(bookActions.LOAD_BOOKRESPONSE)
    // Map the payload into JSON to use as the request body
    .pipe(
      switchMap(() => {
        return this.booksService
          .getBooks()
          .pipe(
            map(books => (books.Page === parseInt('0', 10) && books.Error === '0') ?
             of(new bookActions.LoadBookResponseFail(books)) : new bookActions.LoadBookResponseSuccess(books)
          ),

          );
      })
    );



}
