import { BookResponse } from './../../models/bookResponse.model';
import * as fromBookResponse from '../actions';
import { Book } from '../../models/book.model';


export interface BookResponseState {
  entities:  { [id: number]: Book };
  Error: string;
  Time: number;
  Page: number;
  Total: string;
  loaded: boolean;
  loading: boolean;
}

export interface BookState {
  entities: { [id: number]: Book };
  loaded: boolean;
  loading: boolean;
}

export const initialState: BookResponseState = {
  entities: {},

  Error: '0',
  Time: 0.0,
  Page: 0,
  Total: '0',
  loaded: false,
  loading: false
};

export function reducer(
  state = initialState,
  action: fromBookResponse.BookResponseAction
): BookResponseState {
  switch (action.type) {
    case fromBookResponse.LOAD_BOOKRESPONSE: {
      return {
        ...state,
        loading: true
      };
    }

    case fromBookResponse.LOAD_BOOKRESPONSE_FAIL: {
      const bookResponse = action.payload;
      return {
        ...state,
        loading: false,
        loaded: false
      };
    }

    case fromBookResponse.LOAD_BOOKRESPONSE_SUCCESS: {
      const booksResponse = action.payload;
      let entities = {};
       if (action.payload.Error !== 'Search {query} not found!' && action.payload.Total !== '0' ) {
         entities = booksResponse.Books.reduce(
          (entities: { [id: number]: Book }, book: Book) => {
            return {
              ...entities,
              [book.ID]: book

            };
          },
          {
            ...state.entities,

          }
        );
       }

      return { entities, Error: action.payload.Error, Time: action.payload.Time, Page: action.payload.Page,
         Total: action.payload.Total, loading: false, loaded: true };

    }
  }

  return state;
}

export const getBooksLoading = (state: BookResponseState) => state.loading;
export const getBooksLoaded = (state: BookResponseState) => state.loaded;
export const getBooksEntities = (state: BookResponseState) => state.entities;
