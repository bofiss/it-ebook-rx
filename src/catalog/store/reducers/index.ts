import { CatalogState} from './index';
import * as fromBooks from './books.reducer';
import { ActionReducerMap } from '@ngrx/store/src/models';
import { createFeatureSelector, createSelector } from '@ngrx/store';



export interface CatalogState {
  books: fromBooks.BookResponseState;
}

export const reducers: ActionReducerMap<CatalogState> = {
  books: fromBooks.reducer
};


export const getBooksState = createFeatureSelector<CatalogState>(
   'catalogs'
);
