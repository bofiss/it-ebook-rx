import { createSelector } from '@ngrx/store';
import * as fromFeature from '../reducers';
import * as fromBooks from '../reducers/books.reducer';
import { Book } from '../../models/book.model';

export const getBookState = createSelector(
  fromFeature.getBooksState,
  (state: fromFeature.CatalogState) => state.books
);

export const getBooksLoading = createSelector(
  getBookState,
  fromBooks.getBooksLoading
);
export const getBooksLoaded = createSelector(
  getBookState,
  fromBooks.getBooksLoaded
);
export const getBooksEntities = createSelector(
  getBookState,
  fromBooks.getBooksEntities
);
export const getTotalPage = createSelector(
  getBookState,
  state => { return Math.floor(parseInt(state.Total, 10) /  10);
  });


export const getAllBooks = createSelector(getBooksEntities, entities => {
  return Object.keys(entities).map(id => entities[parseInt(id, 10)]);
});
